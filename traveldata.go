package main

import (
	"net/http"
	"fmt"
	"log"
	"io/ioutil"
	"encoding/json"
	"time"
	"strconv"
	"os"
)


type TravelData struct {
	Routes   []struct {
		Legs []struct {
			DurationInTraffic struct {
				Text string `json:"text"`
				Value int `json:"value"`
			} `json:"duration_in_traffic"`
		} `json:"legs"`
	} `json:"routes"`

	//routes.legs.duration_in_traffic.text = string resp
	//routes.legs.duration_in_traffic.value = int seconds
}

type FormattedTravelData struct {
	Text string
	Value int
	Time string
}

func MakeRequest() ([]byte, error) {
	resp, err := http.Get("https://maps.googleapis.com/maps/api/directions/json?departure_time=now&origin=43.076295,-70.761161&destination=42.360621,-71.113126&key=API_KEY")
	if err != nil {
		log.Fatalln(err) //Log error out if it occurs
	}

	body, err := ioutil.ReadAll(resp.Body) //Put body into an array of bytes

	if err != nil {
		log.Fatalln(err) //Log an error if ioutil can't read this
	}
	//log.Println(string(body)) Will print the raw response out to console
	log.Println("Request made")
	return body, err
}

func getTravelData(body []byte) (*FormattedTravelData, error) {
	var s = new(TravelData)
	err := json.Unmarshal(body, &s)
	if (err != nil){
		log.Fatal("Error unmarshaling JSON: ", err)
	}
	if (s != nil) {

	} else {
		fmt.Println("Empty object")
	}
	var f = new(FormattedTravelData)
	f.Text = s.Routes[0].Legs[0].DurationInTraffic.Text
	f.Value = s.Routes[0].Legs[0].DurationInTraffic.Value
	f.Time = time.Now().Format(time.RFC850)
	return f, err
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	raw_request, err := MakeRequest()
	if (err != nil) {
		log.Fatal("Main function error: ", err)
	}
	ResponseObject, err := getTravelData([]byte(raw_request))
	//TravelDataList := []*FormattedTravelData{ResponseObject} If I wanted a list (slice), I would do this)
	log.Println(ResponseObject)
	dataToWrite := []byte(ResponseObject.Text + "," + strconv.Itoa(ResponseObject.Value) + "," + ResponseObject.Time + "\n")
	f, err := os.OpenFile("traveldata.csv", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	if _, err := f.Write(dataToWrite); err != nil {
		log.Fatal(err)
	}
	if err := f.Close(); err != nil {
		log.Fatal(err)
	}

}
